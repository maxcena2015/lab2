#include <iostream>
#include <vector>
#include <string>

using namespace std;

class LongInt
{
public:
    bool sign;
	vector<short> v;

    LongInt();

    LongInt& operator= (const LongInt&);
	LongInt& operator= (char *s);

	LongInt operator% (const LongInt&);
	LongInt operator% (const int &);
	bool operator< (const LongInt&);
	bool operator<= (const LongInt&);
	bool operator> (const LongInt&);
	bool operator>= (const LongInt&);
	bool operator== (const LongInt&);


	static vector<short> GetSub(vector<short>& v, int index, int length) {
			vector<short> result = vector<short>(length);
			for (size_t i = 0; i < length; i++)
			{
				result[i] = v[index + i];
			}
			return result;
		}

	static LongInt Divide(const LongInt &a,  LongInt &b, LongInt &remainder) {
			if (CompareAbs(a.v, b.v) == -1) {
				remainder = a;
				return 0;
			}
			string result="";
			auto aVCopy = a.v;
			int aSize = a.v.size();
			int bSize = b.v.size();
			int lastIndex = aSize - bSize;
			int index;
			auto subVN = [&]()
			{
				vector<short> sub(0);
				for (int i = lastIndex; i < aSize; i++)
				{
					sub.push_back(aVCopy[i]);
				}
				return LongInt(sub,true);
			};
			while (lastIndex>=0)
			{
				if (CompareAbs(subVN().v, b.v) == -1) {
					result += '0';
				}
				else {
					index = 0;
					for (int i = 2; i <= 10; i++)
					{
						if (CompareAbs(subVN().v, (b*LongInt(i)).v) == -1) {
							index = i - 1;
							break;
						}
					}
					if (index == 0) {
						cout <<"smth went wrong" <<endl;
						exit(1);
					}
					result += to_string(index);
					vector<short> newSubV = (subVN() - b * LongInt(index)).v;
					for (int i = lastIndex; i < aSize; i++)
					{
						if (i - lastIndex < NonZero(newSubV).size())
							aVCopy[i] = newSubV[i - lastIndex];
						else
							aVCopy[i] = 0;
					}
				}
				lastIndex--;
			}
			remainder = LongInt(aVCopy,true);
			return LongInt( NonZero( LongInt(result).v),true);
		}

	static vector<short> PositiveSub(const vector<short>& x1, const  vector<short>& x2) {
		if (CompareAbs(x1, x2) == -1) {
			cout << "x1<x2: " << endl;
			exit(1);
		}
		vector<short> result = vector<short>(0);
		vector<short> x1Copy = x1;
		auto X2 = [&](int index) {
			if (index >= x2.size())
				return (short)0;
			return x2[index];
		};
		auto arrange = [&](int index) {
			if (x1Copy[index] < X2(index)) {
				x1Copy[index] += 10;
				while (x1Copy[++index] == 0)
				{
					x1Copy[index] = 9;
				}
				x1Copy[index]--;
			}
		};
		for (int i = 0; i < x1.size(); i++)
		{
			arrange(i);
			result.push_back(x1Copy[i] - X2(i));
		}
		return result;
	}

	static vector<short> SumNegPos(const vector<short>& x1, const  vector<short>& x2,
		bool& sign) {
		return SumPosNeg(x2, x1, sign);
	}
	static vector<short> SumNegNeg(const vector<short>& x1, const  vector<short>& x2,
		bool& sign) {
		sign = false;
		return SumPosPos(x1, x2);
	}
	static vector<short> SumPosNeg(const vector<short>& x1, const  vector<short>& x2,
		bool& sign) {
		vector<short> result;
		if (CompareAbs(x1, x2) == -1) {
			sign = false;
			result = PositiveSub(x2, x1);
		}
		else {
			sign = true;
			result = PositiveSub(x1, x2);
		}
		return result;
	}

	static vector<short> SumPosPos(const vector<short>& v1, const vector<short>& v2) {
		short toAdd = 0, currAdd = 0;
		vector<short> result = vector<short>(0);
		auto getAt = [](vector<short> v, int index) {
			if (index >= v.size())
				return (short)0;
			return v[index];
		};
		int maxSize = v1.size() > v2.size() ? v1.size() : v2.size();
		for (int i = 0; i < maxSize; i++)
		{
			currAdd = toAdd + getAt(v1, i) + getAt(v2, i);
			if (currAdd < 10)
				toAdd = 0;
			else
			{
				toAdd = 1;
				currAdd -= 10;
			}
			result.push_back(currAdd);
		}
		if (toAdd == 1)
			result.push_back(1);
		return result;
	}

	LongInt(vector<short> v, bool sign) {
		this->v = v;
		this->sign = sign;
	}

	LongInt operator -(LongInt longInt) const {
		return *this + LongInt(longInt.v, !longInt.sign);
	}


	static short Char2Short(char ch) {
		return ch - (short)'0';
	}


	static vector<short> Str2V(string  s) {
		int length = s.length();
		auto result = vector<short>(length);
		for (int i = 0; i < length; i++)
		{
			result[length - 1 - i] = Char2Short(s[i]);
		}
		return result;
	}

	static vector<short>  NonZero(vector<short> v) {
		auto result = vector<short>(1);
		result[0] = v[0];
		int lastNonZero = 0;
		for (size_t i = 0; i < v.size(); i++)
		{
			if (v[i] != 0)
				lastNonZero = i;
		}
		for (size_t i = 1; i <= lastNonZero; i++)
		{
			result.push_back(v[i]);
		}
		return result;
	}

	LongInt operator  +(LongInt num2) const {
		bool local_sign;
		vector<short> value;
		if (sign) {
			if (num2.sign) {
				local_sign = true;
				value = SumPosPos(v, num2.v);
			}
			else {
				value = SumPosNeg(v, num2.v, local_sign);
			}
		}
		else {
			if (num2.sign) {
				value = SumNegPos(v, num2.v, local_sign);
			}
			else {
				value = SumNegNeg(v, num2.v, local_sign);
			}
		}
		return LongInt(NonZero(value), local_sign);
	}

	LongInt operator *(LongInt num2) const {
		return LongInt(NonZero(Multiply(v,num2.v)),num2.sign==sign);
	}

	static int CompareAbs(vector<short> v1, vector<short> v2)
	{
		v1 = NonZero(v1);
		v2 = NonZero(v2);
		if (v1.size() > v2.size())
			return 1;
		else if (v2.size() > v1.size())
			return -1;
		else {
			for (int i = v1.size() - 1; i >= 0; i--)
			{
				if (v1[i] > v2[i])
					return 1;
				else if (v2[i] > v1[i])
					return -1;
			}
			return 0;
		}
	};

	LongInt(string s) {
		if (s[0] == '-') {
			sign = false;
			v = Str2V(s.substr(1));
		}
		else {
			sign = true;
			v = Str2V(s);
		}
	};

	LongInt operator %(LongInt num2)const  {
		LongInt n;
		Divide(*this, num2, n);
		return LongInt(NonZero( n.v),true);
	}

	static LongInt PowMod(LongInt longInt, LongInt pow, LongInt mod) {
		if (CompareAbs(pow.v, LongInt(0).v) == 0)
			return 1;
		if (CompareAbs(pow.v, LongInt(1).v) == 0)
			return longInt % mod;
		if (pow.v[0] % 2 == 0) {
			return (PowMod(longInt, pow / 2, mod) ^ 2) % mod;
		}
		else {
			return (longInt % mod) * (PowMod(longInt, pow / 2, mod) ^ 2) % mod;
		}
	};



	class Fermat {
	public:
		static bool Prime(LongInt n, LongInt a) {
			return CompareAbs(PowMod(a, n - 1, n).v, LongInt(1).v) == 0;
		}
		static bool Prime(LongInt n, int rounds = 1) {
			for (int i = 0; i < rounds; i++)
			{
				if (!Prime(n, LongInt(Get(2, ToInt(n - 1)))))
					return false;
			}
			return true;
		}
	};

	LongInt(int n) :LongInt(to_string(n)) {}

	LongInt operator /(LongInt num2) const {
		LongInt n(1);
		return Divide(*this, num2, n);
	}

	static LongInt Pow(LongInt x, LongInt pow) {
		if (CompareAbs(pow.v, LongInt(0).v) == 0)
			return LongInt(1);
		if (CompareAbs(pow.v, LongInt(1).v) == 0)
			return x;

		if (pow.v[0] % 2 == 0) {
			return Pow(x * x, pow / LongInt(2));
		}
		else {
			return x * Pow(x * x, pow / LongInt(2));
		}
	};

	LongInt operator ^(LongInt pow)const {
		return Pow(*this, pow);
	}

	static int ToInt(LongInt longInt) {
		int n = 0;
		int ntoAdd = 0;
		for (int i = 0; i < longInt.v.size(); i++)
		{
			ntoAdd = 1;
			for (int j = 0; j < i; j++)
			{
				ntoAdd *= 10;
			}
			n += ntoAdd * longInt.v[i];

		}
		return n;
	}

	static int Get(int min, int max) {
			return  min + (rand() % static_cast<int>(max - min + 1));
		}

    static vector<short> Int2Vector(int num) {
			auto v = vector<short>(0);
			size_t length = to_string(num).length();
			for (size_t i = 0; i < length; i++)
			{
				v.push_back(num % 10);
				num /= 10;
			}
			return v;
		}

static vector<short> AddZero(vector<short>& v, int extend) {
			vector<short> result = vector<short>(v.size() + extend);
			for (int i = 0; i < result.size(); i++)
			{
				if (i < v.size())
					result[i] = v[i];
				else
					result[i] = 0;
			}
			return result;
		}

static vector<short> Shift(vector<short> & v, int shift) {
			vector<short> result = vector<short>(shift + v.size());
			for (size_t i = 0; i < shift; i++)
			{
				result[i] = 0;
			}
			for (size_t i = shift; i < result.size(); i++)
			{
				result[i] = v[i - shift];
			}
			return result;
		}

static vector<short> Multiply(vector<short> v1, vector<short> v2) {
			vector<short> result = vector<short>(0);
			v1 = NonZero(v1);
			v2 = NonZero(v2);
			if (v1.size() == 1 && v2.size() == 1) {
				return Int2Vector(v1[0] * v2[0]);
			}
			vector<short> x1, x0, y1, y0, z2, z1, z0;
			int maxEven = v1.size() > v2.size() ? v1.size() : v2.size();
			if (maxEven % 2 == 1)
				maxEven++;
			v1 = AddZero(v1, maxEven - v1.size());
			v2 = AddZero(v2, maxEven - v2.size());
			x0 = GetSub(v1, 0, maxEven / 2);
			x1 = GetSub(v1, maxEven / 2, maxEven / 2);
			y0 = GetSub(v2, 0, maxEven / 2);
			y1 = GetSub(v2, maxEven / 2, maxEven / 2);

			z2 = Multiply(x1, y1);
			z0 = Multiply(x0, y0);

			z1 = Multiply(SumPosPos(x1, x0), SumPosPos(y1, y0));
			z1 = PositiveSub(z1, z2);
			z1 = PositiveSub(z1, z0);

			result = Shift(z2, maxEven);
			result = SumPosPos(result, Shift(z1, maxEven / 2));
			result = SumPosPos(result, z0);

			return result;
		}

	};




int main()
{
    cout << LongInt::Fermat::Prime(1235477, 10) << endl;
    return 0;
}
